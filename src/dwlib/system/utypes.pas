//*******************************************************************
//  ProjectGroup1  pfuzhuang   
//  utypes.pas     
//                                          
//  创建: changym by 2012-01-03 
//        changup@qq.com                    
//  功能说明:
//      自定义类型定义；
//  修改历史:
//                                            
//  版权所有 (C) 2012 by changym
//                                                       
//*******************************************************************
unit utypes;

interface

uses
  Classes, SysUtils, Graphics, ADODB, StdCtrls, UpComboBox, frxDBSet,
  UpListBox, UpAdoConnection, Messages, UPChuangwGroup, UpChuangw,
  UpWWDbGrid, UpAdoQuery, UpAdoStoreProc, UpCheckBox, upchecklistedit,
  RzTreeVw, forms, UpAdvToolPanel;

const

  {*****************************************************************************
                             自定义消息相关部分
  *****************************************************************************}
  //form目标窗体给来源窗体通知数据已更新消息
  UPM_DESFORM2SOURCEFORM_NOTIFIED = WM_USER + 1000;
                  
  { 第一、门诊医生站相关消息;
    发送消息方式：SendMessage(self.Parent.Parent.Handle, UPM_FRMMZ_YISZ, wParam, lParam);
    wParam = 来源窗体标识, lParam: 消息类型; 
  }
  UPM_FRMMZ_YISZ = WM_USER + 100;           //门诊医生站医生工作台窗口自定义消息
  UPM_FRMMZ_YISZ_WP_FRMZHEND = 1001;        //诊断窗体

  UPM_FRMMZ_YISZ_WP_FRMXIYCFD = 1002;       //西医处方窗体  
  UPM_FRMMZ_YISZ_LP_XIYCF_NEW = 201;        //新增西医处方
  UPM_FRMMZ_YISZ_LP_XIYCF_ZUOF = 202;       //作废西医处方

  UPM_FRMMZ_YISZ_WP_FRMZCHENGYCFD = 1003;   //中成药处方窗体
  UPM_FRMMZ_YISZ_LP_ZCHENGYCF_NEW = 301;    //新增中成药处方
  UPM_FRMMZ_YISZ_LP_ZCHENGYCF_ZUOF = 302;   //作废中成药处方

  UPM_FRMMZ_YISZ_WP_FRMZCAOYCFD = 1004;     //中草药处方窗体
  UPM_FRMMZ_YISZ_LP_ZCAOYCF_NEW = 401;      //新增中草药处方
  UPM_FRMMZ_YISZ_LP_ZCAOYCF_ZUOF = 402;     //作废中草药处方

  UPM_FRMMZ_YISZ_WP_FRMJIANCD = 1005;       //检查单窗体
  UPM_FRMMZ_YISZ_LP_JIANCD_NEW = 501;       //新增检查单
  UPM_FRMMZ_YISZ_LP_JIANCD_ZUOF = 502;      //作废检查单

  { 第二、充当父亲窗体的panel大小改变时给儿子们的通知消息;
    窗体收到此消息时根据消息参数带来的width和height调整自身大小即可;
    发送消息方式：Broadcast; 广播消息即可;
    tagMSG.message带去width和height;
    width在低两个字节, height在高两个字节; 
  }
  UPM_PANEL_RESIZE = WM_USER + 101;

  { 第三、mdichild窗体关闭的时候通知mdimain窗体关闭消息;
    myhis主窗体在没有子窗体打开的时候自动显示左边的导航栏;
  }
  UPM_MDICHILD_ONCLOSE = WM_USER + 102;

  { 第四、系统参数修改后通知主调度程序刷新内存数据
    这个时候要使子模块生效,无须重新启动调度应用,只需要重新关闭打开子模块即可
  }
  //全局参数改变了
  UPM_SYSPARAMS_CHANGED_GLOBAL = WM_USER + 103;
  //本机参数改变了
  UPM_SYSPARAMS_CHANGED_LOCAL = WM_USER + 104;

  { 第五、panel中的子窗体关闭的时候通知父窗体关闭消息 }
  UPM_PANEL_SUBFORM_ONCLOSE = WM_USER + 105; 

  { 第六、住院医生站相关消息定义 }
  //住院医生站床位图重画消息
  UPM_FRMZHUY_YISZ_CHUANGWGROUP_REPAINT = WM_USER + 106;

  //查询主窗体和应用主窗体交互消息
  UPM_CXBBMAINV2_OPEN = WM_USER + 107; //窗体打开消息
  UPM_CXBBMAINV2_CLOSE = WM_USER + 108; //窗体关闭消息

  //通知主窗体关闭
  UPM_FRMMAIN_CLOSE = WM_USER + 109; //通知主窗体关闭
                                   
  //通知主窗体关闭左侧导航栏
  UPM_FRMMAIN_LEFTNAVBAR_CLOSE = WM_USER + 110;
type
  PList = ^TList;
  PComboBox = ^TComboBox;
  PUpComboBox = ^TUpComboBox;
  PUpCheckBox = ^TUpCheckBox;
  PUPListBox = ^Tuplistbox;
  PBitMap = ^TBitMap;
  PCanvas = ^TCanvas;
  PAdoConnection = ^TAdoConnection;
  PUpAdoConnection = ^TUpAdoConnection;
  PfrxDBDataset = ^TfrxDBDataset;
  PUPwwDbgrid = ^TupwwdbGrid;
  PUPAdoQuery = ^Tupadoquery;
  PUpAdoStoreProc = ^TUpAdoStoreProc;
  PUPCheckListEdit = ^TUPCheckListEdit;
  PRzCheckTree = ^TRzCheckTree;
  PForm = ^TForm;
  PUpAdvToolPanel = ^TUpAdvToolPanel;

  //窗体编辑状态枚举定义
  //浏览，增加，修改, 自定义的编辑状态
  EFrmEditState = (fesBrower, fesAppend, fesUpdate); 

  {*****************************************************************************
                             系统枚举定义
  *****************************************************************************}
  //性别类型,男/女;
  EXBType = (xbNan = 1, xbNv);

  //医生表/员工类别;医生/护士;
  EYuangLeib = (yglbYis=1, yglbHus, yglbShoufy, yglbOther=0);

  //医生表/状态;正常/停用;
  EYuangZhuangt = (ygztZhengc=1, ygztTingy);

  //床位状态;空闲/使用/停用;
  EChuangwZhuangt = (cwztKongx=0, cwztShiy, cwztTingy);

  //药房药库类型;药库/药房
  EYaofYaokType = (yfykYaok=1, yfykYaof);

  //项目表jc_xiangm类别leib定义,药品\费用\材料\医托
  EXiangmLeib = (xlYaop=1, xlFeiy, xlCail, xlYit);

  //药品分多次使用标识;不允许0;允许当天1;允许跨天2;
  EFendcsybs = (fdcNone=0, fdcCurrDay, fdcAll);


  {*****************************************************************************
                             门诊业务定义部分
  *****************************************************************************} 
  //医生工作台目录树节点; 病历节点;处方单节点;检查单节点;
  EMenZYisgztTreeNode = (menzysttnBingl=-1, menzysttnChufd=-2, menzysttnJiancd=-3);

  //处方单类型定义(西医处方,中成药处方,中草药处方,检查单,处置单,药房划价单)
  EChufLeix = (clnone = 0, clxiychuf=1, clzchengychuf, clzcaoychuf, cljiancdchuf, clchuzd,
      clyaofhjd);

  //门诊费费用单类型(诊断费,西药处方,中成药处方,中草药处方,检查单,处置单)
  EMenzFeiydLeix = (mzfeiydlxZhend=1, mzfeiydlxXiycf, mzfeiydlxZChengycf,
      mzfeiydlxZCaoycf, mzfeiydlxJiancd, mzfeiydlxShuyf, mzfeiydlxPisf,
      mzfeiydlxZhusf, mzfeiydlxHulf);

  //支付方式编码;系统固定项:现金\扣押金\农保\医保\欠费
  EZhifFangsType = (zffsXianj=1,zffsKouyj,zffsNongb,zffsYib,zffsQianf);

  TFunc_onButtonClick = procedure (Sender:TObject) of object;

  //选择记录处理函数
  TfunSelRecord = procedure(bianm : integer) of object;


implementation

end.
