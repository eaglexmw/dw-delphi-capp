unit udmtbl_public;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uDmBase, ADODB, UpAdoStoreProc, DB, UpAdoQuery, utypes, udlltransfer,
  UpComboBox, ubase, uprzbuttonedit, upedit, AdvDateTimePicker, UpListBox;

type
  Tdmtbl_public = class(TDMBase)
    qrylog: TUpAdoQuery;
  private
  { 聚合其他的业务对象 }

  public
    baseobj : tbase;

  { 下拉框combobox填充函数 }
  public
    procedure fillcbb_userTableName(pcbb : PUpComboBox; strtablename : String);
    //根据用户指定的表全表添加
    procedure fillcbb_userTableNameRL(pcbb : PUpComboBox; strtablename : String);
    //fillcbb_userTableName函数的变种, 重新加载字典数据;需要保留itemindex的值
    procedure fillcbb_userSql(pcbb : PUpComboBox; strsql,keyfield,captionfield : string);
    //根据strsql填充cbb,关键字和值字段用户指定
    procedure CbbDictRefresh(strtablename : String; pcbb : PUpComboBox);
    //同fillcbb_userTableNameRL

  { 编码2名称/名称2编码部分 }
  public
    function bianm2mingc(tablename : string; bianm : integer) : string;
    //通用表的bianm2名称函数
    function mingc2bianm(tablename, mingc : string) : Integer;
    //通用表的名称2编码函数
    function bianm2meircs(bianm : Integer) : Integer;
    //用法表通过编码获取每日次数
    function tbl_getFieldValue(strSql : string) : string;
    //根据sql获取字段值

  { 评审相关cbb类填充
  }
  public
    procedure fillCbb_pings_jib(pcbb : PUpComboBox);
    //填充评审级别
    procedure fillcbb_pings_zhuangt(pcbb : PUpComboBox);
    //填充评审状态
    procedure fillcbb_pings_zhuanj_jiaos(pcbb: PUpComboBox);
    //填充专家角色
               
  public
    function getDbServerDateTime() : TDateTime;
    //得到dbServer时间

  { 验证部分
  }
  public
    function cbbRequire(cbb:TUpComboBox; strmsg : string) : boolean;
    //cbb非空验证
    function uprzBtnEditRequire(edt:tuprzbuttonedit; strmsg : string) : boolean;
    //uprzButtonEdit非空验证
    function edtRequire(edt:TUpEdit; strmsg : string) : boolean;
    //edt非空验证
    function updtGtNow(updt:TAdvDateTimePicker; strmsg : string) : boolean;
    //updatetime日期大于当下
    function uplistItemCountGT0(lst : TUpListBox; strmsg :string) : Boolean;
    //lst item.count大于0

  { 验证函数
  }
  public
    function pingslb_jiancxmbIsEmpty(pingslbbm: integer): boolean;
    //指定评审类别下检查项模版是否为空
    function pingslb_MobwjIsEmpty(pingslbbm: integer) : boolean;
    //指定评审类别下模版文件是否为空

  public
    function writeLog(caoz: string; yulzfc1:string='';yulzfc2:string='';yulzfc3: string=''): Boolean;
    //写日志

  public
    constructor Create(pdllparams : PDllParam); override;
    destructor Destroy; override;
  end;

implementation

uses umysystem;

{$R *.dfm}

{ Tdmtbl_public }

procedure Tdmtbl_public.fillcbb_userTableName(pcbb: PUpComboBox;
  strtablename: String);
begin
  //填充患者费用类型
  qry1.Close;
  qry1.SQL.Text := 'select bianm, mingc from ' + strtablename;
  qry1.Open;
  while not qry1.Eof do
  begin
    pcbb^.AddItemUP(qry1.fieldbyname('mingc').AsString,
        qry1.fieldbyname('bianm').AsString);
    qry1.Next;
  end;
  qry1.Close;
end;

procedure Tdmtbl_public.fillcbb_userTableNameRL(pcbb: PUpComboBox;
  strtablename: String);
var
  strval : string;
begin
  strval := pcbb^.GetItemValue;
  pcbb^.ClearItems;
  fillcbb_userTableName(pcbb, strtablename);
  pcbb^.ItemIndex := pcbb^.ValueItemIndex(strval);
end;

function Tdmtbl_public.bianm2mingc(tablename: string;
  bianm: integer): string;
begin
  Result := '';
  qry1.Close;
  qry1.SQL.Text := 'select mingc from ' + tablename +
    ' where bianm=' + IntToStr(bianm);
  qry1.Open;

  if qry1.RecordCount = 0 then
  begin
    qry1.Close;
    Exit;
  end;
  Result := qry1.Fields[0].AsString;
  qry1.Close;
end;

function Tdmtbl_public.mingc2bianm(tablename, mingc: string): Integer;
begin
  Result := 0;
  qry1.Close;
  qry1.SQL.Text := 'select bianm from ' + tablename +
    ' where mingc=''' + mingc + '''';
  qry1.Open;
  if qry1.RecordCount = 0 then
  begin
    qry1.Close;
    Exit;
  end;
  Result := qry1.Fields[0].AsInteger;
  qry1.Close;
end;

constructor Tdmtbl_public.Create(pdllparams: PDllParam);
begin
  inherited Create(pdllparams);

  baseobj := tbase.Create;

  //创建聚合的其他业务对象
end;

destructor Tdmtbl_public.Destroy;
begin
  //销毁聚合的其他业务对象

  freeandnil(baseobj);

  inherited;
end;

procedure Tdmtbl_public.fillcbb_userSql(pcbb: PUpComboBox; strsql,
  keyfield, captionfield: string);
begin
  //填充患者费用类型
  qry1.Close;
  qry1.SQL.Text := strsql;
  qry1.Open;
  while not qry1.Eof do
  begin
    pcbb^.AddItemUP(qry1.fieldbyname(captionfield).AsString,
        qry1.fieldbyname(keyfield).AsString);
    qry1.Next;
  end;
  qry1.Close;

end;

procedure Tdmtbl_public.CbbDictRefresh(strtablename: String;
  pcbb: PUpComboBox);
var
  strval : string;
begin
  strval := pcbb^.GetItemValue;
  pcbb^.ClearItems;
  fillcbb_userTableName(pcbb, strtablename);
  pcbb^.ItemIndex := pcbb^.ValueItemIndex(strval);
end;

function Tdmtbl_public.bianm2meircs(bianm: Integer): Integer;
begin
  Result := 0;

  qry.Close;
  qry.SQL.Text := 'select meircs from jc_yongf where bianm=' + IntToStr(bianm);
  qry.Open;
  if qry.RecordCount = 0 then
  begin
    qry.Close;
    Exit;
  end;
  Result := qry.Fields[0].AsInteger;
  qry.Close;
end;

function Tdmtbl_public.getDbServerDateTime: TDateTime;
begin
  qry.OpenSql('select getdate()');
  Result := qry.Fields[0].AsDateTime;
  qry.Close;
end;

function Tdmtbl_public.tbl_getFieldValue(strSql: string): string;
begin
  Result := '';
  qry.Close;
  qry.SQL.Text := strSql;
  qry.Open;
  if qry.RecordCount > 0 then
    Result := qry.Fields[0].AsString;
  qry.Close;
end;

function Tdmtbl_public.cbbRequire(cbb: TUpComboBox;
  strmsg: string): boolean;
begin
  Result := false;

  if cbb.ItemIndex=-1 then
  begin
    baseobj.showwarning(strmsg);
    cbb.SetFocus;
    exit;
  end;

  Result := true;
end;

function Tdmtbl_public.uprzBtnEditRequire(edt: tuprzbuttonedit;
  strmsg: string): boolean;
begin
  Result := false;

  if Trim(edt.Text) = '' then
  begin
    baseobj.showwarning(strmsg);
    edt.SetFocus;
    exit;
  end;

  Result := true;
end;

function Tdmtbl_public.edtRequire(edt: TUpEdit; strmsg: string): boolean;
begin
  Result := false;

  if Trim(edt.Text) = '' then
  begin
    baseobj.showwarning(strmsg);
    edt.SetFocus;
    exit;
  end;

  Result := true;
end;

function Tdmtbl_public.updtGtNow(updt: TAdvDateTimePicker;
  strmsg: string): boolean;
begin
  Result := false;
  if updt.DateTime <= Now then
  begin
    updt.SetFocus;
    baseobj.showwarning(strmsg);
    exit;
  end;

  Result := true;
end;

function Tdmtbl_public.uplistItemCountGT0(lst: TUpListBox;
  strmsg: string): Boolean;
begin
  Result := false;

  if lst.Items.Count=0 then
  begin
    lst.SetFocus;
    baseobj.showwarning(strmsg);
    exit;
  end;

  Result := true;
end;

procedure Tdmtbl_public.fillCbb_pings_jib(pcbb: PUpComboBox);
begin
  pcbb^.ClearItems;       
  pcbb^.AddItemUP('部门级', '2');
  pcbb^.AddItemUP('所级', '1');
end;

procedure Tdmtbl_public.fillcbb_pings_zhuangt(pcbb: PUpComboBox);
begin
  pcbb^.ClearItems;
  pcbb^.AddItemUP('新建', '0');
  pcbb^.AddItemUP('待审批', '5');
  pcbb^.AddItemUP('专家批注', '10');
  pcbb^.AddItemUP('准备上会文档', '15');  
  pcbb^.AddItemUP('会议准备', '20');
  pcbb^.AddItemUP('评审会', '25');
  pcbb^.AddItemUP('意见落实', '30');
  pcbb^.AddItemUP('确认修改', '35');
  pcbb^.AddItemUP('评审结束', '40');
end;

procedure Tdmtbl_public.fillcbb_pings_zhuanj_jiaos(pcbb: PUpComboBox);
begin
  pcbb^.ClearItems;
  pcbb^.AddItemUP('评审组长', '1');
  pcbb^.AddItemUP('副组长', '2');
  pcbb^.AddItemUP('专家', '3');
end;

function Tdmtbl_public.pingslb_jiancxmbIsEmpty(pingslbbm: integer): boolean;
begin
  qry2.OpenSql(format('select top 1 1 from jc_jiancx_mob' +
    ' where pingslxbm=%d', [pingslbbm]));
  result:= qry2.RecordCount=0;
  qry2.Close;
end;

function Tdmtbl_public.pingslb_MobwjIsEmpty(pingslbbm: integer): boolean;
begin
  qry2.OpenSql(format('select top 1 1 from jc_pingslx_wend_mob' +
    ' where pingslxbm=%d', [pingslbbm]));
  Result:= qry2.RecordCount=0;
  qry2.Close;
end;

function Tdmtbl_public.writeLog(caoz, yulzfc1, yulzfc2,
  yulzfc3: string): Boolean;
begin
  try
    qrylog.ExecuteSql(Format('insert into sys_riz_caoz(chuangklm,caozybm' +
        ',caozyxm,caozsj,caoz,yulzfc1,yulzfc2,yulzfc3)' +
        ' values(''%s'',%d,''%s'',getdate(),''%s'',''%s'',''%s'',''%s'')',
        [LowerCase(self.ClassName),fpdllparams^.mysystem^.loginyuang.bianm,
         fpdllparams^.mysystem^.loginyuang.xingm, caoz, yulzfc1, yulzfc2, yulzfc3]));
    Result:= True;
  except
    on E:Exception do
    begin
      Result:= False;
      errmsg:= e.Message;
    end;
  end;
end;

end.
