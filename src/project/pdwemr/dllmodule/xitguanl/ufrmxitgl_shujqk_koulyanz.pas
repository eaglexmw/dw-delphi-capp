unit ufrmxitgl_shujqk_koulyanz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, Buttons,
  UpSpeedButton, ExtCtrls, UPanel, udmtbl_yis, StdCtrls, UpLabel, UpEdit;

type
  Tfrmxitgl_shujqk_koulingyanz = class(Tfrmdbdialogbase)
    lbl1: TUpLabel;
    lbl2: TUpLabel;
    edtkoul: TUpEdit;
    procedure FormDestroy(Sender: TObject);
    procedure btnyesClick(Sender: TObject);
  private
    //医生对象; 也就是操作员对象;
    dmyis : Tdmtbl_yis;

  protected
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;
  end;

var
  frmxitgl_shujqk_koulingyanz: Tfrmxitgl_shujqk_koulingyanz;

implementation

{$R *.dfm}

{ Tfrmxitgl_shujqk_koulingyanz }

function Tfrmxitgl_shujqk_koulingyanz.Execute_dll_showmodal_before: Boolean;
begin
  //创建医生对象
  dmyis := Tdmtbl_yis.Create(@dllparams);

  Result := True;
end;

procedure Tfrmxitgl_shujqk_koulingyanz.FormDestroy(Sender: TObject);
begin
  inherited;

  //销毁医生对象
  FreeAndNil(dmyis);
end;


procedure Tfrmxitgl_shujqk_koulingyanz.btnyesClick(Sender: TObject);
begin
  //inherited;
  
  //校验输入的管理员口令是否正确
  
  //口令不允许为空
  if Trim(edtkoul.Text) = '' then
  begin
    baseobj.showwarning('请输入管理员口令！');
    Exit;
  end;

  //验证口令; 用户编码=0标识为管理员admin
  if not dmyis.checkPassword('0', edtkoul.Text) then
  begin
    //管理员口令不正确
    baseobj.showerror('管理员口令不正确,请重新输入！');
    edtkoul.Clear;
    Exit;
  end;

  //口令验证通过
  ModalResult := mrOk;
end;

function Tfrmxitgl_shujqk_koulingyanz.Execute_dll_showmodal_after: Boolean;
begin
  Result := ModalResult = mrOk;
end;

end.
