unit ufrmxitgl_shujqk_qingk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbdialogbase, Menus, UpPopupMenu, frxClass, frxDesgn,
  frxDBSet, UpDataSource, DB, ADODB, UpAdoTable, UpAdoQuery, Buttons,
  UpSpeedButton, ExtCtrls, UPanel, udmtbl_xitgl, StdCtrls, UpEdit,
  CnValidateImage, UpLabel, frxExportXLS, frxExportPDF, UpCheckBox;

type
  Tfrmxitgl_qingksj_qingk = class(Tfrmdbdialogbase)
    lbl4: TUpLabel;
    imgyanzm: TCnValidateImage;
    edtyanzm: TUpEdit;
    chkqingkjcxj: TUpCheckBox;
    procedure FormDestroy(Sender: TObject);
    procedure btnyesClick(Sender: TObject);
  private    
    //系统管理对象
    dmxitgl : Tdmtbl_xitgl;

    procedure cleardata;
    //清空数据

  protected
    function Execute_dll_showmodal_before() : Boolean; override;
    function Execute_dll_showmodal_after() : Boolean; override;

  end;

var
  frmxitgl_qingksj_qingk: Tfrmxitgl_qingksj_qingk;

implementation

{$R *.dfm}

{ Tfrmxitgl_qingksj_qingk }

function Tfrmxitgl_qingksj_qingk.Execute_dll_showmodal_before: Boolean;
begin
  //创建系统管理对象
  dmxitgl := Tdmtbl_xitgl.Create(@dllparams);

  Result := True;
end;

procedure Tfrmxitgl_qingksj_qingk.FormDestroy(Sender: TObject);
begin
  inherited;

  //销毁系统管理对象
  FreeAndNil(dmxitgl);
end;

procedure Tfrmxitgl_qingksj_qingk.btnyesClick(Sender: TObject);
begin
  //inherited;
  
  //校验验证码是否正确
  if not imgyanzm.ValidateInput(edtyanzm.Text) then
  begin
    baseobj.showwarning('验证码不正确,看不清请单击图片重新获取验证码！');
    edtyanzm.Clear;
    Exit;
  end;

  //开始清空选择类型的业务数据;
  //再次询问
  if not baseobj.showdialog('数据清空提示', '确认清空选择模块的业务数据吗？') then Exit;
  
  //开始清空
  cleardata();

  ModalResult := mrOk;
end;

procedure Tfrmxitgl_qingksj_qingk.cleardata;
begin
  try
    //启动事务
    dmxitgl.BeginTrans;
    dmxitgl.clearspparams;
    dmxitgl.addspparam_in('@operid', dllparams.mysystem^.loginyuang.bianm);
    if chkqingkjcxj.Checked then
      dmxitgl.addspparam_in('@isclearjicsj', 1)
    else                         
      dmxitgl.addspparam_in('@isclearjicsj', 0);

    dmxitgl.sys_yewsjqk;
    if dmxitgl.getspparam('@ireturn') < 0 then
      raise Exception.Create(dmxitgl.getspparam('@sreturn'));

    //提交事务
    dmxitgl.CommitTrans;

    baseobj.showmessage('业务数据清空成功！');
    edtyanzm.Clear;
  except
    on E:Exception do
    begin
      //回滚事务
      dmxitgl.RollbackTrans;
      baseobj.showerror('清空业务数据失败:' + e.Message);
      Exit;
    end;
  end;
end;

function Tfrmxitgl_qingksj_qingk.Execute_dll_showmodal_after: Boolean;
begin
  Result := True;
end;

end.
