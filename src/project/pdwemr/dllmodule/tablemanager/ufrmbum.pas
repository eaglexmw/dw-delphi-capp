unit ufrmbum;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ufrmdbtreebase, ImgList, frxExportXLS, frxClass, frxExportPDF,
  Menus, UpPopupMenu, frxDesgn, frxDBSet, UpDataSource, DB, ADODB,
  UpAdoTable, UpAdoQuery, ComCtrls, StdCtrls, CnEdit, UpCnedit, UpGroupBox,
  Buttons, UpSpeedButton, ExtCtrls, UPanel, ToolPanels, UpAdvToolPanel;

type
  tfrmbum = class(Tfrmdbtreebase)
  protected
    function getnodename_showtreenode() : string; override;
            
    function addrootnodecheck() : Boolean; override;
    //添加根节点验证
    function addchildnodecheck() : Boolean; override;
    //添加子节点验证
    function UpdateNodeCheck() : Boolean; override;
    //修改节点验证
                           
    function deletecheck() : Boolean; override;
    
    function AddRootNode_SetFields() : Boolean; override;
    //添加根节点设置字段值
    function AddChildNode_Setfields(fujdbm : integer) : Boolean; override;
    //添加子节点设置字段值
    function UpdateNode_SetFields(fujdbm : integer) : Boolean; override;
    //修改节点设置字段值

    function UpdateNodeBefore() : Boolean; override;
    //修改节点前
    function UpdateNodeAfter() : Boolean; override;
    //修改节点成功后,主要用于变动tree的显示
    
    function Execute_dll_showmodal_before() : Boolean; override;  
  end;

var
  frmbum: tfrmbum;

implementation

{$R *.dfm}

function tfrmbum.AddChildNode_Setfields(
  fujdbm: integer): Boolean;
begin
  //增加用户节点赋值
  qrytree.FieldByName('yonghbm').AsString := edtyonghbm.Text;
  
  Result := inherited AddChildNode_Setfields(fujdbm);
end;

function tfrmbum.addchildnodecheck: Boolean;
begin
  Result := inherited addchildnodecheck();
end;

function tfrmbum.AddRootNode_SetFields: Boolean;
begin      
  //增加用户节点赋值                   
  qrytree.FieldByName('yonghbm').AsString := edtyonghbm.Text;
  
  Result := inherited AddRootNode_SetFields;
end;

function tfrmbum.addrootnodecheck: Boolean;
begin
  Result := inherited addrootnodecheck;
end;

function tfrmbum.deletecheck: Boolean;
begin
  //删除前验证检查
  Result := False;
  
  //1. 是否存在下级节点
  if dbcheck.RowIsExists('select top 1 * from jc_bum where fujdbm=' +
      qrytree.fieldbyname('bianm').AsString) then
  begin
    baseobj.showwarning('当前节点存在下级引用,不允许删除！');
    Exit;
  end;

  //2. 是否存在<业务表>的引用
  {
  if dbcheck.RowIsExists('select top 1 * from jc_wuz where fenlbm=' +
      qrytree.fieldbyname('bianm').AsString) then
  begin
    baseobj.showwarning('当前分类存在物资明细,不允许删除！');
    Exit;
  end;
  }

  Result := True;
end;

function tfrmbum.Execute_dll_showmodal_before: Boolean;
begin
  inherited Execute_dll_showmodal_before;
  
  Result := True;
end;

function tfrmbum.getnodename_showtreenode: string;
var
  yonghbm : string;
begin
  yonghbm := qrytree.fieldbyname('yonghbm').AsString;
  if Trim(yonghbm) = '' then
    Result := qrytree.fieldbyname('mingc').AsString
  else
    Result := qrytree.fieldbyname('yonghbm').AsString +
      ' ' + qrytree.fieldbyname('mingc').AsString;
end;

function tfrmbum.UpdateNode_SetFields(
  fujdbm: integer): Boolean;
begin                                                
  //增加用户节点赋值                   
  qrytree.FieldByName('yonghbm').AsString := edtyonghbm.Text;
  
  Result := inherited UpdateNode_SetFields(fujdbm);
end;

function tfrmbum.UpdateNodeAfter: Boolean;
var
  fenlbmqm : string;
  fenlmcqm : string;
begin
  if Trim(edtyonghbm.Text) <> '' then
    tvtree.Selected.Text := edtyonghbm.Text + '-' + edtmc.Text
  else
    tvtree.Selected.Text := edtmc.Text;

  {修复jc_wuz表分类全名字段
  qry2.OpenSql('select bianm,fenlbm from jc_wuz ' +
      ' where fenlbmqm like ''%/' + qrytree.fieldbyname('bianm').AsString +
      '/%''');
  qry2.First;
  while not qry2.Eof do
  begin
    fenlbmqm := getnodefullbm(qry2.getInteger('fenlbm')) + '/';
    fenlmcqm := getnodefullname(qry2.getInteger('fenlbm'));

    qry1.ExecuteSql('update jc_wuz set fenlbmqm=''' + fenlbmqm +
        ''', fenlmcqm=''' + fenlmcqm + '''' +
        ' where bianm=' + qry2.getString('bianm'));

    qry2.Next;
  end;
  qry2.Close; 
  }
end;

function tfrmbum.UpdateNodeBefore: Boolean;
begin
  Result := False;
  if not inherited UpdateNodeBefore then Exit;

  edtyonghbm.Text := qrytree.fieldbyname('yonghbm').AsString;  
  edtmc.text := qrytree.fieldbyname('mingc').asstring;

  Result := True;
end;

function tfrmbum.UpdateNodeCheck: Boolean;
begin
  Result := inherited UpdateNodeCheck;
end;

end.
